import angular from 'angular';

import Utils from './utils.service';
import money from './money.filter';
import timestamp from './timestamp.filter';
import infiniteScroll from './infinite-scroll.directive';

export default angular
  .module('asciiShop.utils', [])
  .service('Utils', ['$timeout', Utils])
  .filter('money', money)
  .filter('timestamp', timestamp)
  .directive('whenScrollEnds', ['$window', 'Utils', infiniteScroll])
  .name;
