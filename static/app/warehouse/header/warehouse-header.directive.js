import WarehouseHeaderCtrl from './warehouse-header.controller';

export default function warehouseHeader() {
  return {
    restrict: 'EA',
    scope: {},
    templateUrl: 'app/warehouse/header/header.html',
    controller: [WarehouseHeaderCtrl],
    controllerAs: 'warehouseHeader',
    replace: true
  };
}
