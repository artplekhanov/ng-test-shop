export default function warehouseProduct() {
  return {
    restrict: 'EA',
    scope: {
      item: '='
    },
    templateUrl: 'app/warehouse/product/product.html',
    replace: true
  };
}
