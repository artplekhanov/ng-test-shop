export default function warehouseContentHeader() {
  return {
    restrict: 'E',
    scope: true,
    templateUrl: 'app/warehouse/content/header/content-header.html',
    require: '^^warehouseContent', // this directive look for the controller on its parents
    replace: true
  };
}
