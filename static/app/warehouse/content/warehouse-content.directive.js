import WarehouseContentCtrl from './warehouse-content.controller';

export default function warehouseContent() {
  return {
    restrict: 'EA',
    scope: {},
    templateUrl: 'app/warehouse/content/content.html',
    controller: ['productsService', 'adsService', WarehouseContentCtrl],
    controllerAs: 'warehouseContent',
    replace: true
  };
}
