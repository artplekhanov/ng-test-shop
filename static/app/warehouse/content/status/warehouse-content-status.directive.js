export default function warehouseContentStatus() {
  return {
    restrict: 'E',
    scope: true,
    templateUrl: 'app/warehouse/content/status/content-status.html',
    require: '^^warehouseContent', // this directive look for the controller on its parents
    replace: true
  };
}
