import angular from 'angular';

import productsService from './warehouse-products.service';

import warehouseContentHeader from './content/header/warehouse-content-header.directive';
import warehouseContentStatus from './content/status/warehouse-content-status.directive';
import warehouseContent from './content/warehouse-content.directive';
import warehouseHeader from './header/warehouse-header.directive';
import warehouseProduct from './product/warehouse-product.directive';

export default angular
  .module('asciiShop.warehouse', [])
  .directive('warehouseContentHeader', warehouseContentHeader)
  .directive('warehouseContentStatus', warehouseContentStatus)
  .directive('warehouseContent', warehouseContent)
  .directive('warehouseHeader', warehouseHeader)
  .directive('warehouseProduct', warehouseProduct)
  .factory('productsService', ['$http', 'Utils', 'AppSettings', productsService])
  .name;
